import { client } from "../src/connection";
import { MeetingDAO } from "../src/daos/meeting-dao";
import { MeetingDaoPostgres } from "../src/daos/meeting-dao-postgres";
import { Meeting } from "../src/entities";



const meetingDAO:MeetingDAO = new MeetingDaoPostgres();

const testMeeting:Meeting = new Meeting(0, "Test Location", "00/00/00", "12:00am", "Test Topics");

test("Create a Meeting", async()=>{
    const result:Meeting = await meetingDAO.createMeeting(testMeeting);
    expect(result.mID).not.toBe(0);
});

test("Get meeting by Id", async ()=>{
    let meeting:Meeting = new Meeting(0, "Test Location 1", "00/00/00", "12:00am", "Test Topics");
    meeting = await meetingDAO.createMeeting(meeting);

    let retrievedMeeting:Meeting = await meetingDAO.getMeetingByID(meeting.mID)

    expect(retrievedMeeting.mLocation).toBe(meeting.mLocation);
});

test("Get all meetings", async ()=>{
    let meeting1:Meeting = new Meeting(0, "Test Location 2", "00/00/00", "12:00am", "Test Topics");
    let meeting2:Meeting = new Meeting(0, "Test Location 3", "00/00/00", "12:00am", "Test Topics");
    let meeting3:Meeting = new Meeting(0, "Test Location 4", "00/00/00", "12:00am", "Test Topics");
    await meetingDAO.createMeeting(meeting1);
    await meetingDAO.createMeeting(meeting2);
    await meetingDAO.createMeeting(meeting3);

    const meetings:Meeting[] = await meetingDAO.getAllMeetings();

    expect(meetings.length).toBeGreaterThanOrEqual(3);
});

test("Update meeting", async ()=>{
    let meeting:Meeting = new Meeting(0, "Test Location 5", "00/00/00", "12:00am", "Test Topics");
    meeting = await meetingDAO.createMeeting(meeting);
    const meetingId = meeting.mID;
    // to update an object we just edit it and then pass it into a method
    meeting.mLocation = "New Test Location";
    meeting = await meetingDAO.updateMeeting(meeting, meetingId);

    const updatedMeeting = await meetingDAO.getMeetingByID(meeting.mID);
    expect(updatedMeeting.mLocation).toBe("New Test Location");
});

test("Delete meeting by id", async ()=>{
    let meeting:Meeting = new Meeting(0, "Test Location 6", "00/00/00", "12:00am", "Test Topics");
    meeting = await meetingDAO.createMeeting(meeting);

    const result:boolean = await meetingDAO.deleteMeeting(meeting.mID);
    expect(result).toBeTruthy()

});


afterAll(async()=>{
    client.end();
});