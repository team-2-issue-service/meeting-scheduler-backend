import { Meeting } from "../entities";

export interface MeetingDAO{

    createMeeting(meeting:Meeting):Promise<Meeting>;

    getAllMeetings():Promise<Meeting[]>;

    getMeetingByID(meetingID:number):Promise<Meeting>;

    updateMeeting(meeting:Meeting, meetingID:number):Promise<Meeting>;

    deleteMeeting(meetingID:number):Promise<boolean>;
}