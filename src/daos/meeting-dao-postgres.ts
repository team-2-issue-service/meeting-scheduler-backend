import { client } from "../connection";
import { Meeting } from "../entities";
import { MissingResourceError } from "../errors";
import { MeetingDAO } from "./meeting-dao";




export class MeetingDaoPostgres implements MeetingDAO{

    async createMeeting(meeting: Meeting): Promise<Meeting> {
        const sql:string= "insert into meeting (m_location, m_date, m_time, m_topics) values ($1,$2,$3,$4) returning m_id";
        const values = [meeting.mLocation, meeting.mDate, meeting.mTime, meeting.mTopics];
        const result = await client.query(sql, values);
        meeting.mID = result.rows[0].m_id;
        return meeting;
    }

    async getAllMeetings(): Promise<Meeting[]> {
        const sql:string = 'select * from meeting';
        const result = await client.query(sql);
        const meetings:Meeting[] =[];
        for(const row of result.rows){
            const meeting:Meeting= new Meeting(
                row.m_id,
                row.m_location,
                row.m_date,
                row.m_time,
                row.m_topics);
                meetings.push(meeting);
        }
        return meetings;
    }

    async getMeetingByID(meetingID:number): Promise<Meeting> {
        const sql:string = 'select * from meeting where m_id = $1';
        const values = [meetingID];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The meeting with id ${meetingID} does not exist`);
        }
        const row = result.rows[0];
        const meeting:Meeting = new Meeting(
                row.m_id,
                row.m_location,
                row.m_date,
                row.m_time,
                row.m_topics);
        return meeting;
    }

    async updateMeeting(meeting: Meeting, meetingID: number): Promise<Meeting> {
        const sql:string = 'update meeting set m_location = $1, m_date = $2, m_time = $3, m_topics = $4 where m_id = $5'
        const values = [meeting.mLocation, meeting.mDate, meeting.mTime, meeting.mTopics, meetingID];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The meeting with id ${meeting.mID} does not exist`);
        }
        return meeting;
    }

    async deleteMeeting(meetingID: number): Promise<boolean> {
        const sql:string = 'delete from meeting where m_id = $1';
        const values = [meetingID];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The meeting with id ${meetingID} does not exist`);
        }
        return true;
    }
}