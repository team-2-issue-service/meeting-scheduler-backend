import { Meeting } from "../entities";


export default interface MeetingService{

    registerMeeting(meeting:Meeting):Promise<Meeting>;

    retrieveAllMeetings():Promise<Meeting[]>;

    retrieveMeetingByID(meetingID:number):Promise<Meeting>;

    modifyMeeting(meeting:Meeting, meetingID:number):Promise<Meeting>;

    removeMeeting(meetingID:number):Promise<boolean>;
}