import { MeetingDAO } from "../daos/meeting-dao";
import { MeetingDaoPostgres } from "../daos/meeting-dao-postgres";
import { Meeting } from "../entities";
import MeetingService from "./meeting-service";




export class MeetingServiceImpl implements MeetingService{

    meetingDAO:MeetingDAO = new MeetingDaoPostgres();

    registerMeeting(meeting:Meeting): Promise<Meeting> {
        return this.meetingDAO.createMeeting(meeting);
    }

    retrieveAllMeetings():Promise<Meeting[]> {
        return this.meetingDAO.getAllMeetings();
    }

    retrieveMeetingByID(meetingID:number): Promise<Meeting> {
        return this.meetingDAO.getMeetingByID(meetingID);
    }

    modifyMeeting(meeting:Meeting, meetingID: number): Promise<Meeting> {
        return this.meetingDAO.updateMeeting(meeting, meetingID);
    }

    removeMeeting(meetingID:number): Promise<boolean> {
        return this.meetingDAO.deleteMeeting(meetingID);
    }
}