import {Client} from 'pg';
require('dotenv').config({path:'D:\\Revature\\Project2\\Scheduler-Backend\\app.env'})

export const client = new Client({
    user:'postgres',
    password:process.env.DBPASSWORD,
    database:process.env.DATABASENAME,
    port:5432,
    host:'34.73.64.214'
})
client.connect();