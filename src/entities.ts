

export class Meeting{
    constructor(
        public mID:number,
        public mLocation:string,
        public mDate:string,
        public mTime:string,
        public mTopics:string
    ){}
}