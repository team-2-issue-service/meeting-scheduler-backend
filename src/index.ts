import express from 'express';
import cors from 'cors';
import { Meeting } from './entities';
import { MissingResourceError } from './errors';
import MeetingService from './services/meeting-service';
import { MeetingServiceImpl } from './services/meeting-service-impl';


const app = express();
app.use(express.json());
app.use(cors());

const meetingService:MeetingService =new MeetingServiceImpl();


// Get meetings
app.get("/meetings", async (req, res)=>{
    const meetings:Meeting[] = await meetingService.retrieveAllMeetings();
    res.send(meetings);
    res.status(200)
});

// Get meeting by Id
app.get("/meetings/:id", async (req, res) =>{
    try{
        const meetingID = Number(req.params.id);
        const meeting:Meeting = await meetingService.retrieveMeetingByID(meetingID);
        res.send(meeting);
    }catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

// Post meeting
app.post("/meetings", async (req, res) =>{
    let meeting:Meeting = req.body;
    meeting = await meetingService.registerMeeting(meeting);
    res.status(201);
    res.send(meeting);
});

// Put meeting
app.put("/meetings/:id", async (req, res) =>{
    try{
        const newMeeting:Meeting = req.body;
        const meetingID = Number(req.params.id);
        const meeting:Meeting = await meetingService.modifyMeeting(newMeeting, meetingID);
        res.send(meeting);
    }catch(error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
})

// Delete meeting
app.delete("/meetings/:id", async (req, res) =>{
    try{
        const meetingID = Number(req.params.id);
        const success:Boolean = await meetingService.removeMeeting(meetingID);
        res.status(205);
        res.send(success);
    }catch(error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});


const PORT = process.env.PORT || 3000;

app.listen(PORT, ()=>{console.log("Application Started")});